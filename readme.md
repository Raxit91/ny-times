# Ny Times

Ny Times is a demo app to showcase the following:

1. MVVM Architecture

2. unit tests

## How to run the project
just clone the project.

open the root directory in android studio 4.2

perform gradle sync

run the app in emulator or device

## How to run the tests
go to the ```androidTest``` dir

open ```ArticleDetailsViewModelTest```

click on the icon between the line number and the class declaration

select the ```Run``` command from the drop down menu

## How to generate coverage reports
ToDo