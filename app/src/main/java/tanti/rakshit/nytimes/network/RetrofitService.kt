package tanti.rakshit.nytimes.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import tanti.rakshit.nytimes.network.model.mostpopulararticles.MostPopularArticleResponse

interface RetrofitService {

    @GET("svc/mostpopular/v2/viewed/1.json?api-key=NBfAhhId9g7VGloMyrs0d12Lan5MFi3D")
    suspend fun getMostPopularArticles() : Response<MostPopularArticleResponse>

    companion object {
        var retrofitService: RetrofitService? = null
        fun getInstance() : RetrofitService {
            if (retrofitService == null) {
                val httpClient = OkHttpClient.Builder()
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
//                interceptor.level = HttpLoggingInterceptor.Level.HEADERS
                httpClient.interceptors().add(interceptor)

                val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.nytimes.com/")
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }

    }
}