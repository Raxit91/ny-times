package tanti.rakshit.nytimes.network.model.mostpopulararticles

import com.google.gson.annotations.SerializedName

data class MostPopularArticleResponse(
    val copyright: String,
    val num_results: Int,
    @SerializedName("results")
    val articles: List<Article>,
    val status: String
)