package tanti.rakshit.nytimes

import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import tanti.rakshit.nytimes.pages.ArticleRepository
import tanti.rakshit.nytimes.pages.articledetails.ArticleDetailsViewModel
import tanti.rakshit.nytimes.pages.articlelist.ArticleListViewModel

class MyViewModelFactory (private val repository: ArticleRepository, private val intentData: Bundle?): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(ArticleListViewModel::class.java)) {
            ArticleListViewModel(this.repository) as T
        } else if (modelClass.isAssignableFrom(ArticleDetailsViewModel::class.java)) {
            ArticleDetailsViewModel(this.intentData) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}