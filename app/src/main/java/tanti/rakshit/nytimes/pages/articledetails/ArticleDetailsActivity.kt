package tanti.rakshit.nytimes.pages.articledetails

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.gson.Gson
import tanti.rakshit.nytimes.MyViewModelFactory
import tanti.rakshit.nytimes.databinding.ActivityArticleDetailsBinding
import tanti.rakshit.nytimes.network.RetrofitService
import tanti.rakshit.nytimes.network.model.mostpopulararticles.Article
import tanti.rakshit.nytimes.pages.ArticleRepository

class ArticleDetailsActivity : AppCompatActivity() {

    var TAG = "ArticleDetailsActivity"

    lateinit var binding: ActivityArticleDetailsBinding
    lateinit var viewModel: ArticleDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArticleDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val retrofitService = RetrofitService.getInstance()
        val mainRepository = ArticleRepository(retrofitService)
        viewModel = ViewModelProvider(this, MyViewModelFactory(mainRepository, intent.extras)).get(ArticleDetailsViewModel::class.java)

        viewModel.article.observe(this, {
            loadArticleData(it)
        })

        viewModel.getArticleDetails()

    }

    private fun loadArticleData(article: Article?) {
        article?.let {
            val toJson = Gson().toJson(it)
            Log.i(TAG, "loadArticleData: " + toJson)
            binding.articleTitle.text = it.title
            binding.articleAbstract.text = it.abstractStr
            binding.articleBy.text = it.byline
            binding.articleDate.text = "Published on: ${it.published_date}"

            if (it.media.isNotEmpty()) {
                val media = it.media[0]
                Glide.with(binding.articleImage).load(media.mediaMetadata.last().url).into(binding.articleImage)
                binding.articleImageCaption.text = media.caption
                binding.articleImageCopyRight.text = "© ${media.copyright}"
                binding.articleMedia.visibility = View.VISIBLE
            } else {
                binding.articleMedia.visibility = View.GONE
            }
        }
    }
}