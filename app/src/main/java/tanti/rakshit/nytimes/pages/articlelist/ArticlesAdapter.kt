package tanti.rakshit.nytimes.pages.articlelist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tanti.rakshit.nytimes.databinding.ItemArticleBinding
import tanti.rakshit.nytimes.network.model.mostpopulararticles.Article

class ArticlesAdapter : RecyclerView.Adapter<ArticleViewHolder>() {

    var articlesList = mutableListOf<Article>()
    var articleSelected: ((Int) -> Unit)? = null

    fun setArticles(articles: List<Article>) {
        this.articlesList = articles.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemArticleBinding.inflate(inflater, parent, false)
        return ArticleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        val article = articlesList[position]
        holder.binding.articleTitle.text = article.title
        holder.binding.articleBy.text = article.byline
        holder.binding.articleDate.text = article.published_date
        holder.binding.root.setOnClickListener {
            articleSelected?.invoke(position)
        }
//        Glide.with(holder.itemView.context).load(movie.imageUrl).into(holder.binding.imageview)
    }

    fun getArticleAt(position: Int): Article {
        return articlesList[position]
    }

    override fun getItemCount(): Int {
        return articlesList.size
    }
}

class ArticleViewHolder(val binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root)