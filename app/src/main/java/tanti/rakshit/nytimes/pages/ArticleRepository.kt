package tanti.rakshit.nytimes.pages

import tanti.rakshit.nytimes.network.RetrofitService

class ArticleRepository (private val retrofitService: RetrofitService) {

    suspend fun getMostPopularArticles() = retrofitService.getMostPopularArticles()

}