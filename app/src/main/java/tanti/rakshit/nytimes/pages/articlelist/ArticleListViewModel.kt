package tanti.rakshit.nytimes.pages.articlelist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import tanti.rakshit.nytimes.network.model.mostpopulararticles.Article
import tanti.rakshit.nytimes.pages.ArticleRepository

class ArticleListViewModel (private val articleRepository: ArticleRepository) : ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val articleList = MutableLiveData<List<Article>>()
    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val loading = MutableLiveData<Boolean>()

    fun getMostPopularArticles() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = articleRepository.getMostPopularArticles()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    articleList.postValue(response.body()?.articles)
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }

    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}