package tanti.rakshit.nytimes.pages.articlelist

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import tanti.rakshit.nytimes.MyViewModelFactory
import tanti.rakshit.nytimes.databinding.ActivityMainBinding
import tanti.rakshit.nytimes.network.RetrofitService
import tanti.rakshit.nytimes.pages.ArticleRepository
import tanti.rakshit.nytimes.pages.articledetails.ArticleDetailsActivity
import tanti.rakshit.nytimes.pages.articledetails.ArticleDetailsViewModel

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: ArticleListViewModel
    private val adapter = ArticlesAdapter()
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val retrofitService = RetrofitService.getInstance()
        val mainRepository = ArticleRepository(retrofitService)
        binding.recyclerview.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerview.adapter = adapter

        viewModel = ViewModelProvider(this, MyViewModelFactory(mainRepository, intent.extras)).get(ArticleListViewModel::class.java)

        viewModel.articleList.observe(this, {
            adapter.setArticles(it)
        })

        viewModel.errorMessage.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.loading.observe(this, Observer {
            if (it) {
//                binding.progressDialog.visibility = View.VISIBLE
            } else {
//                binding.progressDialog.visibility = View.GONE
            }
        })

        viewModel.getMostPopularArticles()

        adapter.articleSelected = this::articleSelected

    }

    fun articleSelected(position: Int) {
        startActivity(Intent(this, ArticleDetailsActivity::class.java).apply {
            putExtra(ArticleDetailsViewModel.EXTRA_ARTICLE_DATA, adapter.getArticleAt(position))
        })
    }

}