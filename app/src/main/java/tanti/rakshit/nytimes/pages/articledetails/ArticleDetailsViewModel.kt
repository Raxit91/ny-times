package tanti.rakshit.nytimes.pages.articledetails

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import tanti.rakshit.nytimes.network.model.mostpopulararticles.Article

class ArticleDetailsViewModel(private val intentData: Bundle?) : ViewModel() {

    val article = MutableLiveData<Article>()

    companion object {
        val EXTRA_ARTICLE_DATA = "EXTRA_ARTICLE_DATA"
    }

    fun getArticleDetails() {
        article.postValue(intentData?.getParcelable(EXTRA_ARTICLE_DATA))
    }

}