package tanti.rakshit.nytimes.pages.articledetails

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.core.os.bundleOf
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tanti.rakshit.nytimes.getOrAwaitValue1
import tanti.rakshit.nytimes.network.model.mostpopulararticles.Article
import tanti.rakshit.nytimes.pages.articledetails.ArticleDetailsViewModel.Companion.EXTRA_ARTICLE_DATA

@RunWith(AndroidJUnit4::class)
class ArticleDetailsViewModelTest {

    private lateinit var viewModel: ArticleDetailsViewModel

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val article = Gson().fromJson(
            """
            {"abstract":"Like some social media trends, this one has its problems, including stomach aches, diarrhea and bad teeth, health experts said.","adx_keywords":"Honey;Video Recordings, Downloads and Streaming;Social Media;TikTok (ByteDance)","asset_id":100000007910875,"byline":"By Eduardo Medina","des_facet":["Honey","Video Recordings, Downloads and Streaming","Social Media"],"eta_id":0,"geo_facet":[],"id":100000007910875,"media":[{"approved_for_syndication":1,"caption":"Dave Ramirez taking a bite of frozen honey on TikTok. “I’m not going to lie,” he said. “That was pretty refreshing.”","copyright":"Left, Audra Melton for The New York Times; right Dave Ramirez","media-metadata":[{"format":"Standard Thumbnail","height":75,"url":"https://static01.nyt.com/images/2021/08/07/multimedia/07XP-TikTok-Honey-Combo/07XP-TikTok-Honey-Combo-thumbStandard.jpg","width":75},{"format":"mediumThreeByTwo210","height":140,"url":"https://static01.nyt.com/images/2021/08/07/multimedia/07XP-TikTok-Honey-Combo/07XP-TikTok-Honey-Combo-mediumThreeByTwo210.jpg","width":210},{"format":"mediumThreeByTwo440","height":293,"url":"https://static01.nyt.com/images/2021/08/07/multimedia/07XP-TikTok-Honey-Combo/07XP-TikTok-Honey-Combo-mediumThreeByTwo440.jpg","width":440}],"subtype":"photo","type":"image"}],"nytdsection":"u.s.","org_facet":["TikTok (ByteDance)"],"per_facet":[],"published_date":"2021-08-08","section":"U.S.","source":"New York Times","subsection":"","title":"TikTok Trend: Eating Frozen Honey and Risking Ill Effects","type":"Article","updated":"2021-08-09 14:59:03","uri":"nyt://article/4d3ef1bb-0cc2-598b-b4d2-ad87269cf420","url":"https://www.nytimes.com/2021/08/08/us/frozen-honey-challenge-tiktok.html"}
        """.trimIndent(), Article::class.java
        )
        val bundle = bundleOf(EXTRA_ARTICLE_DATA to article)
        viewModel = ArticleDetailsViewModel(bundle)
    }

    @Test
    fun testArticleDetailsViewModel() {
        viewModel.getArticleDetails()
        val abstractStr = viewModel.article.getOrAwaitValue1().abstractStr
        assertThat(abstractStr == "Like some social media trends, this one has its problems, including stomach aches, diarrhea and bad teeth, health experts said.").isTrue()
    }

}